package oata;


/**
 * @author Mikita Mironenka
 * runner class
 */
public class HelloWorld {

    /**
     * Constructor
     */
    private HelloWorld(){}

    /**
     * Message of result
     */
    private static final String MESSAGE = "HELLO ANT";

    /**
     * Method for ant task
     * Print message in console if everything is clear
     * @param args not used
     */
    public static void main(String[] args) {

        System.out.println(MESSAGE);
    }
}
